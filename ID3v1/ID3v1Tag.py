# -*- coding: utf-8 -*-

from MusicMetadataReader.constants import ID3V10, ID3V11, ID3V12, ID3V1E
import logging

logger = logging.getLogger(__name__)

# TODO what with None. eg not defined in version

# TODO define constants for different versions
SUBVERSIONS = [ID3V10, ID3V11, ID3V12, ID3V1E]
FIELD_LENGTH = dict()
FIELD_LENGTH[ID3V10] = {"title": 30, "artist": 30, "album": 30, "year": 4, "comment": 30}
FIELD_LENGTH[ID3V11] = {"title": 30, "artist": 30, "album": 30, "year": 4, "comment": 28, "track":1}
# TODO length comment/track
FIELD_LENGTH[ID3V12] = {"title": 60, "artist": 60, "album": 60, "year": 4, "comment": 43, "track": 1, "subgenre": 20}
FIELD_LENGTH[ID3V1E] = {"title": 90, "artist": 90, "album": 90, "year": 4, "comment": 28, "track": 1, "speed": 1, "subgenre": 30, "start_time": 6, "end_time": 6}

class ID3v1Tag(object):

    """docstring for ID3v1."""

    def __init__(self, version, title, artist, album, year, comment, track,
            genre, subgenre = None, speed = None, start_time = None,
            end_time = None, no_check = False):
        if no_check or __check_input(subversion):
            super(ID3v1Tag, self).__init__()
            self.__title = title
            self.__artist = artist
            self.__album = album
            self.__year = year
            self.__comment = comment
            self.__track = track
            if (genre < 0 or genre > 191):
                self.__genre = 255
            else:
                self.__genre = int(genre)
            self.__subgenre = subgenre
            self.__speed = speed
            self.__start_time = start_time
            self.__end_time = end_time
            self.__subversion = version
        else:
            logger.error("Invalid arguents for creation ID3v1 Tag.")
            raise Exception("Invalid arguments for creation ID3v1 Tag")

    def get_subversion(self):
        return self.__subversion

    def set_subversion(self, subversion):
        if subversion in SUBVERSIONS:
            self.__subversion = subversion
        else:
            logger.error("{} is not a valid subversion for ID3v1".format(subversion))

    def get_title(self):
        return self.__title.decode('iso-8859-1').rstrip(b"\x00")
    # TODO: what if title is None
    def set_title(self, title):
        if not isinstance(title,bytes):
            title = text.encode('iso-8859-1')
        field_length = self.__get_field_length("title")
        _bytes = __resize_argument(title, field_length)
        self.__title = _bytes

    def get_artist(self):
        return self.__artist.decode('iso-8859-1').rstrip(b"\x00")

    def set_artist(self, artist):
        if not isinstance(artist,bytes):
            artist = text.encode('iso-8859-1')
        field_length = self.__get_field_length("artist")
        _bytes = __resize_argument(_bytes, field_length)
        self.__artist = _bytes

    def get_album(self):
        return self.__album.decode('iso-8859-1').rstrip(b"\x00")

    def set_album(self,album):
        if not (isinstance(album,bytes)):
            album = text.encode('iso-8859-1')
        field_length = self.__get_field_length("album")
        _bytes = __resize_argument(album, field_length)
        self.__album = _bytes

    def get_year(self):
        return self.__year.decode('iso-8859-1')

    def set_year(self):
        pass

    def get_comment(self):
        pass

    def set_comment(self,comment):
        pass

    def get_track(self):
        pass

    def set_track(self):
        pass

    def set_genre(self):
        pass

    def get_subgenre(self):
        pass

    def set_subgenre(self):
        pass

    def get_speed(self):
        pass

    def set_speed(self):
        pass

    def get_start_time(self):
        pass

    def set_start_time(self,start_time):
        pass

    def get_end_time(self):
        pass

    def set_end_time(self,end_time):
        pass

    def __get_field_length(self, field):
        return FIELD_LENGTH[self.__subversion][field] if (field in FIELD_LENGTH[self.__subversion]) else None

    def __str__(self):
        global genre_list
        result = u'{ Title: ' + self.__title + u', Artist: ' + self.__artist + \
            u', Album: '+self.__album+u', Year: '+self.__year+u', Comment: ' + \
            self.___comment
        if self.__track:
            result += u', Track: '+self.__track
        if self.__genre == 255:
            result += u', Genre: Other(255)'
        else:
            result += u', Genre: {0}({1:d})'.format(genre_list[self.__genre], self.__genre)
        if self.__subgenre:
            result += u', Subgenre: '+self.__subgenre
        result += u'}'
        return result


def __check_input(subversion, arguments):
    # check max length
    pass


def __resize_argument(argument, length):
    padding = b"\x00"
    argument += padding*(length-len(argument))
    return argument[:min(len(argument), length)]
