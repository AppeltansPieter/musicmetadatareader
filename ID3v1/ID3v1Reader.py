# -*- coding: utf-8 -*-


import os
import MusicMetadataReader.ID3v1.ID3v1Tag as ID3v1Tag
from MusicMetadataReader.constants import ID3V10, ID3V11, ID3V12, ID3V1E

import logging

logger = logging.getLogger(__name__)


# Input: the file object
# Output:
#   - None: if file does not have an ID3V1 tag
#   - ID3v1 Tag
def read_ID3v1(file, filename):
    global logger
    file_size = os.stat(filename).st_size  # get size of file
    if (file_size < 128):  # Check if file has at least 128 bytes -> if not return None (file can not contain ID3v1 tag)
        return None
    try:
        file.seek(-128,os.SEEK_END)  # Move pointer to 128 bytes from end of file (this is the expected start position of
            # the ID3v1 tag)
        if (file.read(3) != "TAG"): # check if
            logger.warning("{0}: No ID3v1 tag found. \n".format(filename))
            return None
        # pointer postion -125
        data = file.read(125)
        title, artist, album, year, comment, track, genre, is_v11 = __read_ID3v11(data)
        if(file_size < 256): # ID3V1.2 is 256 bytes long
            is_v12 = False
        else:
            file.seek(-256, os.SEEK_END)
            is_v12 = (file.read(3) == "EXT")
        if is_v12:
            # pointer position is -253
            data = file.read(125)
            add_title, add_artist, add_album, add_comment, subgenre = __read_ID3v12(data)
            title = title+add_title
            artist = artist + add_artist
            album = album + add_album
            comment = comment + add_comment
            is_enhanced = False
        else:
            if(file_size<355): # ID3v1-Enhanced is 355 bytes long
                is_enhanced = False
            else:
                file.seek(-355,os.SEEK_END)
                is_enhanced  = (file.read(4).decode('iso-8859-1') == "TAG+")
            if is_enhanced:
                data = file.read(223)
                add_title, add_artist, add_album, speed, subgenre, \
                    speed, start_time, end_time = __read_ID3v1Enhanced(data)
                title = title+add_title
                artist = artist + add_artist
                album = album + add_album
        if not is_v11 and not is_v12 and not is_enhanced:
            subversion = ID3V10
            return ID3v1Tag.ID3v1Tag(subversion, title, artist, album, year,
            comment, track, genre)
        elif not is_v12 and not is_enhanced:
            subversion = ID3V11
            return ID3v1Tag.ID3v1Tag(subversion, title, artist, album,year,
            comment, track, genre)
        elif is_v12:
            subversion = ID3V12
            return ID3v1Tag.ID3v1Tag(subversion, title, artist, album, year,
            comment, track, genre, subgenre)
        else:
            subversion = ID3V1E
            return ID3v1Tag.ID3v1Tag(subversion, title, artist, album, year,
                comment, track, genre, subgenre, speed,start_time,end_time)

    except Exception as e:
        return None
    pass

def __read_ID3v11(data):
    assert(type(data) is bytes)
    assert(len(data) == 125)
    # ID3v11 tag format
    # TODO webaddress standard definition
    # -128   |-125 (30)  |-95 (30) |-65 (30) | -35(4)| -31 (30)              | -1 (1)
    #   T  AG|  title    |  artist |  album  |  year | comment (/\x00 track) | genre
    #
    title = data[:30]
    artist = data[30:60]
    album = data[60:90]
    year = data[90:94]
    s_data = data[94:124]
    if s_data[28] == b'\x00':
        comment = s_data[0:28]
        track = ord(s_data[29])
        is_v11 = True
    else:
        comment = s_data
        track = None
        is_v11 = False
    genre = ord(data[124])
    return title, artist, album, year, comment, track, genre, is_v11


def __read_ID3v12(data):
    assert(type(data) is bytes)
    assert(len(data) == 125)
    # ID3v12 format
    # TODO webaddress standard definition
    # -256   |-253 (30)   | -223 (30)   | -193(30)   |              |
    #   E  XT| add. title | add. artist | add. album | add. comment | subgenre
    add_title = data[:30]
    add_artist = data[30:60]
    add_album = data[60:90]
    add_comment = data[90:105]
    subgenre = data[105:125]
    return add_title, add_artist, add_album, add_comment, subgenre


def __read_ID3v1Enhanced(data):
    assert(type(data) is bytes)
    assert(len(data) == 223)
    # ID3v1-enhanced
    # -355   | -351 (60)  | -291 (60)   | -231 (60)  | -171 | -170 (30) | -140 (6)  | -134 (6)
    #   T AG+| add. title | add. artist | add. album | speed| subgenre  | start_time| end_time
    add_title = data[:60]
    add_artist = data[60:120]
    add_album = data[120:180]
    speed = ord(data[180:181])
    subgenre = data[181:211]
    start_time = data[211:217]
    end_time = data[217:223]
    return add_title, add_artist, add_album, speed, subgenre, speed, start_time, end_time
