import MusicMetadataReader.ID3v1.ID3v1Reader as ID3v1Reader
import os.path
import MusicMetadataReader.constants as constants
import logging

logger = logging.getLogger(__name__)


class MusicFile(object):
    """docstring for MusicFile."""

    def __init__(self, filelocation):
        super(MusicFile, self).__init__()
        self.__file_location = filelocation
        self.__last_edit = os.path.getmtime(self.__file_location)
        self.__containers = dict()
        self.__read_meta_data()

    # Which metadata containers are in the file
    def list_metadata_containers():
        pass

    # Returns True if the file has a ID3V1 tag
    def has_ID3V1_container(self):
        if (self.__is_modified()):
            self.__read_meta_data()
        return "ID3V1" in self.__containers.keys()

    def has_ID3V2_container(self):
        pass

    def has_vorbis_comments_container(self):
        pass

    def has_APEV1_container(self):
        pass

    def get_property():
        pass

    # PRIVATE FUNCTIONS
    # Check whether the file was modified on disk since last read metadata
    def __is_modified(self):
        return self.__last_edit != os.path.getmtime(self.__file_location)

    def __read_meta_data(self):
        # Look for the different meta data containers and read them
        # STEP 1: look for ID3V1 container
        #   If found read add to self.__container dictionary
        with open(self.__file_location, 'rb') as file:
            ID3v1 = ID3v1Reader.read_ID3v1(file, self.__file_location)
            if ID3v1 is not None:
                self.__containers[constants.ID3V1] = ID3v1
        pass
