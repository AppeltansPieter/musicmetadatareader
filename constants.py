ID3V1 = "ID3v1"
# ID3v1 subversions:
ID3V10 = "ID3v1.0"
ID3V11 = "ID3v1.1"
ID3V12 = "ID3v1.2"
ID3V1E = "ID3v1-Enhanced"

ID3V2 = "ID3v2"
# ID3v2 subversions
ID3V22 = "ID3v2.2"
ID3V23 = "ID3v2.3"
ID3V24 = "ID3v2.4"

# Properies
TITLE = "title"
ARTIST = "artist"
ALBUM = "album"
