import os
import binascii
import ID3v2
# TODO add robuustness against faulty files

DEBUG = True
def mp3_ID3v2_read_header(mp3_file):
    global DEBUG
    mp3_file.seek(0,0)
    if(mp3_file.read(3) == b'\x49\x44\x33'):
        #speed up? mp3_file.read(2) == 'b\x03\x00'
        version= binascii.hexlify(mp3_file.read(2))

        flag_byte = mp3_file.read(1)
        unsynchronisation = (ord(flag_byte) & 0x80 == 0x80)
        if (version == '0200'):
            compression = (ord(flag_byte) & 0x40 == 0x40)
            extended_header = False
        else:
            extended_header = (ord(flag_byte) & 0x40 == 0x40)
        experimental = (ord(flag_byte) & 0x20 == 0x20)
        size_flags = mp3_file.read(4)
        size = (ord(size_flags[0]) & 0x7f)*2**21+(ord(size_flags[1]) & 0x7f)*2**14+(ord(size_flags[2]) & 0x7f)*2**7+(ord(size_flags[3]) & 0x7f)
        header = {"version":version,"unsynchronisation":unsynchronisation,"compression":compression,"size":size}
    else:
        if DEBUG:
            print "No ID3v2 TAG found"
        return None

def read_ID3v22(mp3_file):
    global DEBUG
    header = mp3_ID3v2_read_header(mp3_file)
    if(header and header.get("version","") == "0200"):
        size = header.get("size",0)
        data = mp3_file.read(size)
        pointer = 0
        while (pointer < size): # -7 ofzo om onvolledige laatste header te vermijden
            frame_identifier,frame_size = ID3v22_read_frameheader(data[pointer:pointer+6])
            pointer += 6
            data[pointer:pointer+frame_size]
    else:
        return None
def read_ID3v23():
    pass
def ID3v2_read_extended_header(data):
    pass

def ID3v23_read_frameheader(header):
    frame_identifier = header[0:4]
    frame_size = header[4:8]
    flags = header[8:10]
    return frame_identifier,frame_size,flags


def ID3v22_read_frameheader(header):
    frame_identifier = header[0:3]
    frame_size = header[3:6]
    return frame_identifier,frame_size

def ID3v22_parse_URL_frame(frame):
    link = frame.decode('iso-8859-1')
    link_end = link.find('\x00')
    if(link_end != -1):
        link = link[:link_end]
    return link
def not_uniqe(frame_identifier):
    list = ["TXX","WAR","WCM","WXX"]
    return frame_identifier in list
# TODO WAR
def ID3v22_is_text_information_frame(frame_identifier):
    return frame_identifier[0] == "T" && frame_identifier != "TXX"
#TODO user defined text frame (TXX) -> mogelijks meer dan 1
# FORMAT: TXX $xx xx xx (size) $xx (encoding) Description
def ID3v22_parse_text_information_frame(frame):
    global ID3v2.text_encoding_identifiers
    text_encoding = ID3v2.text_encoding_identifiers.get(frame[0:2],'iso-8859-1')
    text = frame[2:].decode('text_encoding')
    text_end = text.find('\x00')
    if (text_end !=-1):
        text = text[:text_end]
    return text
    # TODO zoek voor nul termination string

def uncode_unsynchronisation():
    pass
def uncode_CRC(data,CRC_data):
    pass
