class ID3v22(object):
    """docstring for ID3v2."""
    __properties = dict()
    def __init__(self, properties): # properties is dictionary met eigenschappen
        super(ID3v2, self).__init__()
        self.__properties = properties
        self.__recommended_buffer_size = properties.pop('BUF','')
        self.__play_counter = properties.pop('CNT','')
        self.__comments = properties.pop('COM','')
        self.__audio_encryption = properties.pop('CRA','')
        self.__encrypted_meta_frame = properties.pop('CRM','')
        self.__event_timing_codes = properties.pop('ETC','')
        self.__equalization = properties.pop('EQU','')
        self.__involved_people_list = properties.pop('IPL','')
        self.__linked_information = properties.pop('LNK','')
        self.__music_cd_identifier = properties.pop('MCI','')
        self.__mpeg_location_lookup_table = properties.pop('MLL','')
        self.__attached_picture = properties.pop('PIC','')
        self.__popularimeter = properties.pop('POP','')
        self.__reverb = properties.pop('REV','')
        self.__relative_volume_adjustment = properties.pop('RVA','')
        self.__synchronized_lyric = properties.pop('SLT','')
        self.__synced_tempo_codes = properties.pop('STC','')
        self.__album = properties.pop('TAL','')
        TBP = properties.pop('TBP','')
        self.__bpm = int(TBP) if #can convert to int else None
        self.__composer = properties.pop('TCM','').split('/')
        # TODO? genre?
        self.__content_type = properties.pop('TCO','')
        # Steeds printen met (C) voor
        self.__copyright_message = properties.pop('TCR','')
        # 4 characters long DDMM
        self.__date = properties.pop('TDA','')
        # Must be numeric value
        self.__playlist_delay = properties.pop('TDY','')
        self.__encoded_by = properties.pop('TEN','')
        self.__file_type = properties.pop('TFT','')
        # TODO 4 characters long HHMM
        self.__time = properties.pop('TIM','')
        self.__initial_key = properties.pop('TKE','')
        # TODO ISO-639-2 standard
        self.__language = properties.pop('TLA','')
        # TO DO
        self.__media_type = properties.pop('TMT','')
        self.__original_artist = properties.pop('TOA','').split('/')
        self.__original_filename = properties.pop('TOF','')
        self.__original_text_writer = properties.pop('TOL','').split('/')
        self.__original_year = properties.pop('TOR','')
        self.__orignal_album = properties.pop('TOT','')
        self.__lead_performer = properties.pop('TP1','').split('/')
        self.__band = properties.pop('TP2','')
        self.__conductor = properties.pop('TP3','')
        self.__remixed_by = properties.pop('TP4','')
        self.__part_of_set = properties.pop('TPA','')
        self.__publisher = properties.pop('TPB','')
        self.__ISRC = properties.pop('TRC','')
        # Complement of TYE,TDA and TIM frames
        self.__recording_dates = properties.pop('TRD','')
        self.__track_number = properties.pop('TRK','')
        # Size of audiofile excluding TAG must be integer
        # TODO signal if invalid
        self.__size = properties.pop('TSI','')
        self.__encoding_software = properties.pop('TSS','')
        self.__content_group_description = properties.pop('TT1','')
        self.__title = properties.pop('TT2','')
        self.__subtitle = properties.pop('TT3','')
        self.__text_writer = properties.pop('TXT','').split('/')
        self.__user_defined_text = properties.pop('TXX','')
        # TODO 4 characters long
        self.__year = properties.pop('TYE','')
        self.__unique_file_identifier = properties.pop('UFI','')
        self.__unsynchronized_lyrics = properties.pop('ULT','')
        # TODO alles met W
        self.__other = properties
    def content_type(self):
        # Niet heel duidelijk
        pass
    def __str__(self):
        result = ''
        return result
    def parse():
        pass
text_encoding_identifiers = {b'\x00':'iso-8859-1',b'\x01':'utf-16'}
